#!/bin/bash
# Optimization of the Intmax problem, L = 1000, x in [0,100].
#
# Epigenetic Algorithm (epiGA)
#
# Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
# In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
# doi> 10.1016/j.ins.2017.10.005
#

java -cp epiGA.jar org.epiga.problem.intmax.IntmaxEpiGA 200000 100 1 0.015 0.25 0 1000 0 100