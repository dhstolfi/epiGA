#!/bin/bash
# Optimization of the Onemax problem, L = 100000.
#
# Epigenetic Algorithm (epiGA)
#
# Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
# In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
# doi> 10.1016/j.ins.2017.10.005
#

java -cp epiGA.jar org.epiga.problem.onemax.OnemaxEpiGA 1000 100 1 0.85 0.5 4 100000 
