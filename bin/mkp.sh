#!/bin/bash
# Optimization of one MKP instance of the OR-Library (J.E. Beasley, 1990).
#
# Epigenetic Algorithm (epiGA)
#
# Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
# In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
# doi> 10.1016/j.ins.2017.10.005
#
# java -cp epiGA.jar org.epiga.problem.mkp.MKPEpiGA for params description.
# try: java -cp epiGA.jar org.epiga.problem.mkp.MKPEpiGA 1000000 100 1 0.015 0.10 1 mknapcb1.txt N (N in [1-30])
#

java -cp epiGA.jar org.epiga.problem.mkp.MKPEpiGA 1000000 100 1 0.015 0.10 1 mknapcb1.txt 1
