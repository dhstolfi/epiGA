/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.epiga.problem.Problem;
import org.epiga.representation.Representation;

/**
 * Epigenetic Population.
 * 
 * @author Daniel H. Stolfi
 * @param <P> the problem type
 * @param <R> the representation type
 * 
 */
public class EpigeneticPopulation<P extends Problem<R>, R extends Representation<?>>
		extends ArrayList<EpigeneticIndividual<P, R>> implements Comparable<EpigeneticIndividual<P, R>> {
	private static final long serialVersionUID = 1L;

	private ExecutorService executor = null;

	/**
	 * Constructor
	 * 
	 */
	public EpigeneticPopulation() {
		super();
	}

	/**
	 * Constructor
	 * 
	 * @param problem     the problem
	 * @param individuals the number of individuals in the population
	 * @param cells       the number of cells in each individual
	 * @param cores       the number of cores available for evaluating the cells
	 * @throws InterruptedException if interrupted while waiting
	 */
	public EpigeneticPopulation(P problem, int individuals, int cells, int cores) throws InterruptedException {
		super(individuals);

		if (cores == 1) {
			// Sequential
			for (int i = 0; i < individuals; i++) {
				add(new EpigeneticIndividual<P, R>(problem, cells));
			}
		} else {
			// Parallel
			executor = Executors.newFixedThreadPool(cores);
			for (int i = 0; i < individuals; i++) {
				Runnable worker = new Generator(problem, cells);
				executor.execute(worker);
			}
			executor.shutdown();
			executor.awaitTermination(1, TimeUnit.HOURS);
		}

	}

	/**
	 * Duplicates the population.
	 * 
	 * @return a new cloned population.
	 */
	public EpigeneticPopulation<P, R> duplicate() {
		EpigeneticPopulation<P, R> temp = new EpigeneticPopulation<>();
		for (Iterator<EpigeneticIndividual<P, R>> iterator = this.iterator(); iterator.hasNext();) {
			EpigeneticIndividual<P, R> individual = iterator.next();
			temp.add(individual.duplicate());
		}
		return temp;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < this.size(); i++) {
			EpigeneticIndividual<?, ?> individual = this.get(i);
			str.append("Individual " + i + "\n");
			str.append(individual);
		}
		return str.toString();
	}

	/**
	 * Returns the best individual in the population.
	 * 
	 * @return the best individual in the population.
	 */
	public EpigeneticIndividual<P, R> getBestIndividual() {
		EpigeneticIndividual<P, R> best = this.get(0);
		for (EpigeneticIndividual<P, R> individual : this) {
			if (individual.isBetter(best)) {
				best = individual;
			}
		}
		return best;
	}

	/**
	 * Returns the best cell in the population.
	 * 
	 * @return the best cell in the population.
	 */
	public EpigeneticCell<P, R> getBestCell() {
		return getBestIndividual().getBestCell();
	}

	/**
	 * Returns the best fitness and the average fitness of the population.
	 * 
	 * @return the best fitness and the average fitness of the population.
	 */
	public double[] getMetrics() {
		Double avgF = 0.0d;
		for (EpigeneticIndividual<P, R> individual : this) {
			Double fitness = individual.getBestCell().getFitness();
			avgF += fitness;
		}
		avgF /= this.size();
		return new double[] { getBestCell().getFitness(), avgF };
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getBestIndividual() == null) ? 0 : getBestIndividual().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (o.getClass().equals(this.getClass()) && (o instanceof EpigeneticPopulation<?, ?>)) {
			if (size() != ((EpigeneticPopulation<?, ?>) o).size()) {
				return false;
			} else {
				EpigeneticPopulation<?, ?> p2 = ((EpigeneticPopulation<?, ?>) o).duplicate();
				EpigeneticPopulation<?, ?> p1 = duplicate();
				Collections.sort(p1);
				Collections.sort(p2);
				int i = 0;
				boolean eq = true;
				while (eq && i < size()) {
					eq = p1.get(i).equals(p2.get(i));
					i++;
				}
				return eq;
			}
		}
		return false;
	}

	@Override
	public int compareTo(EpigeneticIndividual<P, R> o) {
		return getBestCell().compareTo(o.getBestCell());
	}

	/**
	 * Internal Class used to parallelize the creation of new individuals
	 * 
	 * @author Daniel H. Stolfi
	 * 
	 */
	class Generator implements Runnable {

		private P problem;
		private int cells;

		/**
		 * Constructor.
		 * 
		 * @param problem the problem
		 * @param cells   the number of cells
		 */
		public Generator(P problem, int cells) {
			this.problem = problem;
			this.cells = cells;
		}

		@Override
		public void run() {
			EpigeneticIndividual<P, R> i = new EpigeneticIndividual<>(problem, cells);
			synchronized (this) {
				add(i);
			}
		}
	}

}
