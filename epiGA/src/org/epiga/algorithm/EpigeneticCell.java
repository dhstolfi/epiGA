/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.algorithm;

import java.util.BitSet;

import org.epiga.problem.Problem;
import org.epiga.random.RandomGenerator;
import org.epiga.representation.Representation;

/**
 * Epigenetic Cell.
 * 
 * @author Daniel H. Stolfi
 * @param <P> the problem type
 * @param <R> the representation type
 */
public class EpigeneticCell<P extends Problem<R>, R extends Representation<?>>
		implements Comparable<EpigeneticCell<P, R>> {

	private P problem;
	private R representation;
	private BitSet nucleosomes;
	private double fitness;

	/**
	 * Constructor
	 * 
	 * @param problem the problem
	 */
	public EpigeneticCell(P problem) {
		this.problem = problem;
		this.nucleosomes = new BitSet(problem.getSize());
		representation = problem.generateSolution();
		fitness = problem.getFitness(representation);
	}

	/**
	 * Constructor. Clones parameters to avoid collateral effects.
	 * 
	 * @param problem        the problem
	 * @param representation the representation
	 * @param n              the nucleosomes
	 */
	@SuppressWarnings("unchecked")
	public EpigeneticCell(P problem, R representation, BitSet n) {
		this.problem = problem;
		this.representation = (R) representation.duplicate();
		this.nucleosomes = (BitSet) n.clone();
		fitness = problem.getFitness(representation);
	}

	/**
	 * Constructor. Clones parameters to avoid collateral effects and assigns a
	 * fitness value.
	 * 
	 * @param problem        the problem
	 * @param representation the representation
	 * @param n              the nucleosomes
	 * @param fitness        the fitness
	 */
	@SuppressWarnings("unchecked")
	public EpigeneticCell(P problem, R representation, BitSet n, double fitness) {
		this.problem = problem;
		this.representation = (R) representation.duplicate();
		this.nucleosomes = (BitSet) n.clone();
		this.fitness = fitness;
	}

	/**
	 * Duplicates the Cell.
	 * 
	 * @return a new cloned cell.
	 */
	public EpigeneticCell<P, R> duplicate() {
		return new EpigeneticCell<>(problem, representation, nucleosomes, fitness);
	}

	/**
	 * Returns <b>true</b> if the cell is better than the <i>other</i>.
	 * 
	 * @param other the other cell.
	 * @return <b>true</b> if the cell is better than the <i>other</i>.
	 */
	public boolean isBetter(EpigeneticCell<P, R> other) {
		return problem.isBetter(fitness, other.fitness);
	}

	/**
	 * Returns the cell's fitness
	 * 
	 * @return the cell's fitness
	 */
	public double getFitness() {
		return fitness;
	}

	@Override
	public int compareTo(EpigeneticCell<P, R> o) {
		return problem.compare(fitness, o.fitness);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((representation == null) ? 0 : representation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (obj.getClass().equals(this.getClass()) && (obj instanceof EpigeneticCell<?, ?>)) {
			EpigeneticCell<?, ?> c = (EpigeneticCell<?, ?>) obj;
			return representation.equals(c.representation);
		}
		return false;
	}

	/**
	 * @return the problem
	 */
	public P getProblem() {
		return problem;
	}

	/**
	 * @return the representation
	 */
	public R getRepresentation() {
		return representation;
	}

	/**
	 * @return the n
	 */
	public BitSet getNucleosomes() {
		return nucleosomes;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder("x: ");
		str.append(representation);
		str.append(" : " + fitness + "\nn: ");
		for (int i = 0; i < representation.getSize(); i++) {
			str.append(nucleosomes.get(i) ? "1" : "0");
		}
		str.append('\n');
		return str.toString();

	}

	/**
	 * Generates a new configuration of the nucleosomes
	 * 
	 * @param pn     the nucleosome probability
	 * @param radius the maximum radius
	 */
	public void generateNucleosomes(float pn, int radius) {
		nucleosomes.clear();
		int size = problem.getSize();
		int i = 0;
		while (i < size) {
			if (RandomGenerator.nextFloat() < pn) {
				int ini = i - radius;
				int end = i + radius;
				for (int j = ini; j <= end; j++) {
					if (j >= 0 && j < size) {
						nucleosomes.set(j);
					}
				}
				i = end;
			}
			i++;
		}
	}

	/**
	 * Cells's crossover. Changes both cells. DOES NOT compute the new fitness
	 * values
	 * 
	 * @param other the other Cell
	 */
	public void crossover(EpigeneticCell<P, R> other) {
		nucleosomes.or(other.nucleosomes);
		other.nucleosomes = nucleosomes;
		problem.crossover(representation, other.representation, nucleosomes);
		fitness = 0.0;
		other.fitness = 0.0;
	}

	/**
	 * Cells's methylation. Computes the new cell's fitness
	 * 
	 * @param eprobability the epigenetic probability
	 */
	public void methylate(float eprobability) {
		problem.methylate(representation, nucleosomes, eprobability);
		fitness = problem.getFitness(representation);
	}

	/**
	 * Updates the contents of the cell with the <i>other</i> passed as parameter.
	 * 
	 * @param other the other cell.
	 */
	@SuppressWarnings("unchecked")
	public void update(EpigeneticCell<P, R> other) {
		problem = other.problem;
		nucleosomes = (BitSet) other.nucleosomes.clone();
		representation = (R) other.representation.duplicate();
		fitness = other.fitness;
	}

}
