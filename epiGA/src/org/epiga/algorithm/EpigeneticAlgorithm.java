/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.algorithm;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Calendar;

import org.epiga.operators.EpigeneticOperator;
import org.epiga.operators.NucleosomesGenerator;
import org.epiga.operators.ReplacementOperator;
import org.epiga.operators.ReproductionOperator;
import org.epiga.operators.SelectionOperator;
import org.epiga.problem.Problem;
import org.epiga.random.RandomGenerator;
import org.epiga.representation.Representation;

/**
 * Epigenetic Algorithm.
 * 
 * @author Daniel H. Stolfi
 * @param <P> the problem type
 * @param <R> the representation type
 * 
 */
public class EpigeneticAlgorithm<P extends Problem<R>, R extends Representation<?>> {

	private SelectionOperator<P, R> selection = new SelectionOperator<>();
	private NucleosomesGenerator<P, R> nucleosomes = new NucleosomesGenerator<>();
	private ReproductionOperator<P, R> crossover = new ReproductionOperator<>();
	private EpigeneticOperator<P, R> epigenetic = new EpigeneticOperator<>();
	private ReplacementOperator<P, R> replacement = new ReplacementOperator<>();

	private int nIndividuals;
	private int nCells;
	private float pe;
	private float pn;
	private int radius;
	private FileWriter f;
	private String filename;
	private P problem;
	private EpigeneticPopulation<P, R> pop; // Individuals
	private long ini;
	private int gen;
	private int verbosity;
	private static final DecimalFormat df = new DecimalFormat("########0.000");
	private static final DecimalFormat memf = new DecimalFormat("0");
	private static final String SEP = ",";

	/**
	 * Creates a new Epigenetic Algorithm instance.
	 * 
	 * @param problem      the problem
	 * @param nIndividuals the number of individuals
	 * @param nCells       the number of cells
	 * @param pe           the epigenetic probability
	 * @param pn           the nucleosome probability
	 * @param radius       the nucleosome radius
	 * @throws EpigeneticException if there is an error in the parameters
	 */
	public EpigeneticAlgorithm(P problem, int nIndividuals, int nCells, float pe, float pn, int radius)
			throws EpigeneticException {
		if (nIndividuals < 2 || nIndividuals % 2 != 0) {
			throw new EpigeneticException("N_Individuals >= 2 and an even number.");
		}
		if (nCells < 1) {
			throw new EpigeneticException("N_Cells >= 1.");
		}
		this.problem = problem;
		this.nIndividuals = nIndividuals;
		this.nCells = nCells;
		this.pe = pe;
		this.pn = pn;
		this.radius = radius;

		System.out.println("EPIGENETIC ALGORITHM:");
		System.out.println("Individuals: " + nIndividuals);
		System.out.println("Cells: " + nCells);
		System.out.println("Pe: " + pe);
		System.out.println("Pn: " + pn);
		System.out.println("Radius: " + radius);
		System.out.println("\nPROBLEM:\n" + problem);
		System.out.println("\nSYSTEM:");
		System.out.println("Available Cores: " + Runtime.getRuntime().availableProcessors());
		System.out.print("JVM Memory: " + memf.format(Runtime.getRuntime().freeMemory() / 1024.0));
		System.out.println("/" + memf.format(Runtime.getRuntime().totalMemory() / 1024.0) + " MBytes");
		long maxMemory = Runtime.getRuntime().maxMemory();
		System.out.println("Total Memory: "
				+ (maxMemory == Long.MAX_VALUE ? "no limit" : memf.format(maxMemory / 1024.0) + " MBytes"));
		System.out.println();

	}

	/**
	 * Executes the algorithm and saves statistics
	 * 
	 * @param nEvaluations the number of evaluation or 0
	 * @param nSeconds     for the optimization process or 0
	 * @param cores        number of parallel cores
	 * @param seed         random seed or <b>null</b> for using current milliseconds
	 * @param outfile      the output file for the best solution
	 * @param statfile     the output file for statistics
	 * @param verbosity    the verbosity 0: nothing, 1: steps, 2: Stats, 3: Initial
	 *                     and Final, 4: Cells, 5: Population
	 * @return an array containing the best fitness and the average fitness of the
	 *         population
	 * @throws IOException          if there is a file error
	 * @throws InterruptedException if a thread is interrupted
	 * @throws EpigeneticException  if there is an algorithm error
	 */
	public double run(int nEvaluations, int nSeconds, int cores, Long seed, String outfile, String statfile,
			int verbosity) throws IOException, InterruptedException, EpigeneticException {

		if (cores < 1) {
			throw new EpigeneticException("Cores > 0.");
		}
		if (nEvaluations <= 0 && nSeconds <= 0) {
			throw new EpigeneticException("Specify N_Evaluations or N_Seconds.");
		}

		this.verbosity = verbosity;
		ini = Calendar.getInstance().getTimeInMillis();
		if (seed == null) {
			seed = Long.valueOf(ini);
		}
		RandomGenerator.init(seed.longValue());

		System.out.println("RUN:");
		System.out.println("Evaluations: " + nEvaluations);
		System.out.println("Seconds: " + nSeconds);
		System.out.println("Cores: " + cores);
		System.out.println("Seed: " + seed.longValue());
		System.out.println("Stats: " + statfile);
		System.out.println("Out: " + outfile);
		System.out.println("Verbosity: " + verbosity);
		System.out.println();

		writeHeader(statfile);

		gen = 0;
		long nMilliseconds = nSeconds * 1000L;

		// Initialization of individuals with N_Cells in each
		pop = new EpigeneticPopulation<>(problem, nIndividuals, nCells, cores);
		writeStat();
		if (verbosity > 2) {
			debug(pop, "INIT");
		}

		while ((nMilliseconds == 0 || Calendar.getInstance().getTimeInMillis() - ini < nMilliseconds)
				&& (nEvaluations == 0 || problem.getEvaluations() < nEvaluations)) {

			// Selection
			EpigeneticPopulation<P, R> temp = selection.select(pop);
			if (verbosity > 3)
				debug(temp, "SELECTION");

			// Nuclesomes
			nucleosomes.generate(temp, pn, radius);
			if (verbosity > 3)
				debug(temp, "NUCLEOSOMES");

			// Crossover
			crossover.reproduction(temp);
			if (verbosity > 3)
				debug(temp, "CROSSOVER");

			// Epigenetics
			epigenetic.mechanism(temp, pe, cores);
			if (verbosity > 3)
				debug(temp, "EPIGENETIC");

			// Replacement: Elitism
			pop = replacement.replace(pop, temp);
			if (verbosity > 3)
				debug(pop, "REPLACEMENT");

			if (verbosity == 1) {
				System.out.print('.');
			}

			gen++;

			writeStat();

		}

		if (verbosity == 1) {
			System.out.println();
		}
		if (verbosity > 2) {
			debug(pop, "END");
		}
		writeClose();

		double[] metrics = pop.getMetrics();
		System.out.println("# Generations: " + gen);
		System.out.println("# Evaluations: " + problem.getEvaluations());
		System.out.println("Best Fitness: " + metrics[0]);
		System.out.println("Average Fitness: " + metrics[1]);
		long millis = Calendar.getInstance().getTimeInMillis() - ini;
		long second = (millis / 1000) % 60;
		long minute = (millis / (1000 * 60)) % 60;
		long hour = Math.floorDiv(millis, (1000 * 60 * 60));
		long mill = millis - hour * 1000 * 60 * 60 - minute * 1000 * 60 - second * 1000;
		System.out.println("Elapsed Time: " + String.format("%02d:%02d:%02d:%03d", hour, minute, second, mill));

		if (outfile != null) {
			problem.save(pop.getBestCell().getRepresentation(), outfile);
		}

		return metrics[0];
	}

	private void writeHeader(String statfile) throws IOException {
		filename = statfile;
		if (statfile != null) {
			f = new FileWriter(statfile);
			f.write("time,generations,evaluations,fitness,average\n");
			if (verbosity > 0) {
				System.out.println("Writing in " + statfile);
			}
		}
	}

	private void writeStat() throws IOException {
		if (f != null || verbosity > 1) {
			String time = Long.toString(Calendar.getInstance().getTimeInMillis() - ini);
			double[] metrics = pop.getMetrics();
			if (verbosity > 1) {
				String str = "Time: " + (time + "            ").substring(0, 12);
				str += " Gen: " + (gen + "      ").substring(0, 6);
				str += " Ev: " + (problem.getEvaluations() + "            ").substring(0, 12);
				str += " Best: " + (df.format(metrics[0]) + "             ").substring(0, 13);
				str += " Avg: " + (df.format(metrics[1]) + "             ").substring(0, 13);
				System.out.println(str);
			}
			if (f != null) {
				f.write(time + SEP + gen + SEP + problem.getEvaluations() + SEP + metrics[0] + SEP + metrics[1] + "\n");
			}
		}
	}

	private void writeClose() throws IOException {
		if (f != null) {
			f.close();
			if (verbosity > 0) {
				System.out.println(filename + " written");
			}
		}
		filename = null;
	}

	private void debug(EpigeneticPopulation<P, R> pop, String label) {
		System.out.println(label);
		double[] metrics = pop.getMetrics();
		System.out.println("Best: " + metrics[0] + " Avg: " + metrics[1]);
		if (verbosity > 4) {
			System.out.println(pop);
		} else {
			System.out.println(pop.getBestCell());
		}
	}

	/**
	 * Entry point
	 * 
	 * @param args no args here
	 */
	public static void main(String[] args) {
		System.out.println("Epigenetic Algorithm (epiGA) v1.0\n");
		System.out.println(
				"Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics.");
		System.out.println(
				"In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018. doi> 10.1016/j.ins.2017.10.005\n");
		System.out.println("Demos included: IntMax, MKP, OneMAX.");
		System.exit(0);
	}

}
