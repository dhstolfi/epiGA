/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.algorithm;

import java.util.ArrayList;
import java.util.Iterator;

import org.epiga.problem.Problem;
import org.epiga.representation.Representation;

/**
 * Epigenetic Individual.
 * 
 * @author Daniel H. Stolfi
 * @param <P> the problem type
 * @param <R> the representation type
 */
public class EpigeneticIndividual<P extends Problem<R>, R extends Representation<?>>
		extends ArrayList<EpigeneticCell<P, R>> implements Comparable<EpigeneticIndividual<P, R>> {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 * 
	 */
	public EpigeneticIndividual() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param problem the problem.
	 * @param size    the number of cells in the individual.
	 */
	public EpigeneticIndividual(P problem, int size) {
		super(size);
		for (int i = 0; i < size; i++) {
			add(new EpigeneticCell<P, R>(problem));
		}
	}

	/**
	 * Duplicates the individual.
	 * 
	 * @return a new cloned individual.
	 */
	public EpigeneticIndividual<P, R> duplicate() {
		EpigeneticIndividual<P, R> ind = new EpigeneticIndividual<>();
		for (Iterator<EpigeneticCell<P, R>> iterator = this.iterator(); iterator.hasNext();) {
			EpigeneticCell<P, R> cell = iterator.next();
			ind.add(cell.duplicate());
		}
		return ind;
	}

	/**
	 * Returns <b>true</b> if the best cell of the individual is better than the
	 * best in <i>other</i>.
	 * 
	 * @param other the other individual.
	 * @return <b>true</b> if the best cell in the individual is better than the
	 *         best in <i>other</i>.
	 */
	public boolean isBetter(EpigeneticIndividual<P, R> other) {
		return getBestCell().isBetter(other.getBestCell());
	}

	/**
	 * Returns the best cell in the individual.
	 * 
	 * @return the best cell in the individual.
	 */
	public EpigeneticCell<P, R> getBestCell() {
		EpigeneticCell<P, R> best = null;
		for (Iterator<EpigeneticCell<P, R>> iterator = this.iterator(); iterator.hasNext();) {
			EpigeneticCell<P, R> cell = iterator.next();
			if (best == null || cell.isBetter(best)) {
				best = cell;
			}
		}
		return best;
	}

	/**
	 * Sets the best cell in the individual.
	 * 
	 * @param cell the cell which replaces the current best cell.
	 */
	public void setBestCell(EpigeneticCell<P, R> cell) {
		EpigeneticCell<P, R> best = getBestCell();
		best.update(cell);
	}

	/**
	 * Individual's crossover. Changes both Individuals.
	 * 
	 * @param other the other Individual
	 */
	public void crossover(EpigeneticIndividual<P, R> other) {
		EpigeneticCell<P, R> c1 = getBestCell();
		EpigeneticCell<P, R> c2 = other.getBestCell();
		c1.crossover(c2);
		setBestCell(c1);
		other.setBestCell(c2);
	}

	/**
	 * Individual's crossover. Changes both Individuals.
	 * 
	 * @param eprobability the epigenetic probability
	 */
	public void methylate(float eprobability) {
		for (Iterator<EpigeneticCell<P, R>> iterator = this.iterator(); iterator.hasNext();) {
			EpigeneticCell<P, R> cell = iterator.next();
			cell.methylate(eprobability);
		}
	}

	@Override
	public int compareTo(EpigeneticIndividual<P, R> o) {
		return getBestCell().compareTo(o.getBestCell());
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < size(); i++) {
			str.append("Cell " + i + "\n");
			str.append(get(i));
		}
		return str.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getBestCell() == null) ? 0 : getBestCell().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (obj.getClass().equals(this.getClass()) && (obj instanceof EpigeneticIndividual<?, ?>)) {
			EpigeneticIndividual<?, ?> ind = (EpigeneticIndividual<?, ?>) obj;
			return getBestCell().equals(ind.getBestCell());
		}
		return false;
	}
}
