/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.operators;

import java.util.Collections;

import org.epiga.algorithm.EpigeneticIndividual;
import org.epiga.algorithm.EpigeneticPopulation;
import org.epiga.problem.Problem;
import org.epiga.representation.Representation;

/**
 * Replacement Operator.
 * 
 * @author Daniel H. Stolfi
 * @param <P> problem type
 * @param <R> representation type
 */
public class ReplacementOperator<P extends Problem<R>, R extends Representation<?>> implements Operator {

	/**
	 * Replaces the individuals in the population in an elitist way.
	 * 
	 * @param pop  the current population
	 * @param inds the new individuals
	 * @return the new population
	 */
	public EpigeneticPopulation<P, R> replace(EpigeneticPopulation<P, R> pop, EpigeneticPopulation<P, R> inds) {

		EpigeneticPopulation<P, R> temp = new EpigeneticPopulation<>();

		for (int i = 0; i < pop.size(); i++) {
			EpigeneticIndividual<P, R> ind = pop.get(i);
			if (!temp.contains(ind)) {
				temp.add(ind);
			}
		}
		for (int i = 0; i < inds.size(); i++) {
			EpigeneticIndividual<P, R> ind = inds.get(i);
			if (!temp.contains(ind)) {
				temp.add(ind);
			}
		}
		Collections.sort(temp);
		if (temp.size() > pop.size()) {
			temp.subList(pop.size(), temp.size()).clear(); // discard worst individuals
		} else if (temp.size() < pop.size()) {
			P problem = temp.get(0).getBestCell().getProblem();
			int isize = temp.get(0).size();
			for (int i = temp.size(); i < pop.size(); i++) {
				temp.add(new EpigeneticIndividual<P, R>(problem, isize)); // fill with random individuals
			}
		}
		return temp;
	}

}
