/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.operators;

import org.epiga.algorithm.EpigeneticIndividual;
import org.epiga.algorithm.EpigeneticPopulation;
import org.epiga.problem.Problem;
import org.epiga.representation.Representation;

/**
 * Reproduction Operator.
 * 
 * @author Daniel H. Stolfi
 * @param <P> problem type
 * @param <R> representation type
 */
public class ReproductionOperator<P extends Problem<R>, R extends Representation<?>> implements Operator {

	/**
	 * Performs an epigenetic crossover of the individuals in the population.
	 * 
	 * @param inds the population
	 */
	public void reproduction(EpigeneticPopulation<P, R> inds) {

		for (int j = 0; j < inds.size(); j = j + 2) {

			EpigeneticIndividual<P, R> i1 = inds.get(j);
			EpigeneticIndividual<P, R> i2 = inds.get(j + 1);
			i1.crossover(i2);
		}
	}

}
