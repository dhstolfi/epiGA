/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.operators;

import org.epiga.algorithm.EpigeneticCell;
import org.epiga.algorithm.EpigeneticIndividual;
import org.epiga.algorithm.EpigeneticPopulation;
import org.epiga.problem.Problem;
import org.epiga.representation.Representation;

/**
 * Nucleosome Generator.
 * 
 * @author Daniel H. Stolfi
 * @param <P> the problem type
 * @param <R> the representation type
 * 
 */
public class NucleosomesGenerator<P extends Problem<R>, R extends Representation<?>> implements Operator {

	/**
	 * Generates the nucleosome vector.
	 * 
	 * @param inds         the population
	 * @param nprobability the nucleosome probability
	 * @param radius       the nucleosome radius
	 */
	public void generate(EpigeneticPopulation<P, R> inds, float nprobability, int radius) {

		for (int j = 0; j < inds.size(); j++) {
			EpigeneticIndividual<P, R> in = inds.get(j);
			for (EpigeneticCell<P, R> cell : in) {
				cell.generateNucleosomes(nprobability, radius);
			}
		}
	}
}
