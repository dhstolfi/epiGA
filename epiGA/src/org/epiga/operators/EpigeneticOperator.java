/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.operators;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.epiga.algorithm.EpigeneticIndividual;
import org.epiga.algorithm.EpigeneticPopulation;
import org.epiga.problem.Problem;
import org.epiga.representation.Representation;

/**
 * Epigenetic Operator.
 * 
 * @author Daniel H. Stolfi
 * @param <P> the problem type
 * @param <R> the representation type
 * 
 */
public class EpigeneticOperator<P extends Problem<R>, R extends Representation<?>> implements Operator {

	private ExecutorService executor = null;

	/**
	 * Applies an epigenetic mechanism.
	 * 
	 * @param inds         the population
	 * @param eprobability the epigenetic probability
	 * @param cores        the number of execution threads
	 * @throws InterruptedException if a thread is interrupted
	 */
	public void mechanism(EpigeneticPopulation<P, R> inds, float eprobability, int cores) throws InterruptedException {

		if (cores == 1) {
			// Sequential
			for (int i = 0; i < inds.size(); i++) {
				inds.get(i).methylate(eprobability);
			}
		} else {
			// Parallel
			executor = Executors.newFixedThreadPool(cores);
			for (int i = 0; i < inds.size(); i++) {
				Runnable worker = new Methylation(inds.get(i), eprobability);
				executor.execute(worker);
			}
			executor.shutdown();
			executor.awaitTermination(1, TimeUnit.HOURS);
			executor = null;
		}
	}

	/**
	 * Implements multithread methylation.
	 * 
	 * @author Daniel H. Stolfi
	 *
	 */
	class Methylation implements Runnable {

		private EpigeneticIndividual<P, R> ind;
		private float eprobability;

		/**
		 * Methylation.
		 * 
		 * @param ind          the individual.
		 * @param eprobability the epigenetic probability.
		 */
		public Methylation(EpigeneticIndividual<P, R> ind, float eprobability) {
			this.ind = ind;
			this.eprobability = eprobability;
		}

		public void run() {
			ind.methylate(eprobability);
		}
	}

}
