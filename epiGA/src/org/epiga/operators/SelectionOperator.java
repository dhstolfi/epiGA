/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.operators;

import org.epiga.algorithm.EpigeneticIndividual;
import org.epiga.algorithm.EpigeneticPopulation;
import org.epiga.problem.Problem;
import org.epiga.random.RandomGenerator;
import org.epiga.representation.Representation;

/**
 * Selection Operator.
 * 
 * @author Daniel H. Stolfi
 * @param <P> problem type
 * @param <R> representation type
 * 
 */
public class SelectionOperator<P extends Problem<R>, R extends Representation<?>> implements Operator {

	/**
	 * Selects a temporary population for working with.
	 * 
	 * @param inds the base population
	 * @return a temporary population for working with.
	 */
	public EpigeneticPopulation<P, R> select(EpigeneticPopulation<P, R> inds) {

		EpigeneticPopulation<P, R> temp = new EpigeneticPopulation<>();

		for (int j = 0; j < inds.size(); j++) {
			EpigeneticIndividual<P, R> i1 = inds.get(RandomGenerator.nextInt(inds.size()));
			EpigeneticIndividual<P, R> i2 = inds.get(RandomGenerator.nextInt(inds.size()));
			// Binary tournament
			if (i1.isBetter(i2)) {
				temp.add(i1.duplicate());
			} else {
				temp.add(i2.duplicate());
			}
		}
		return temp;
	}

}
