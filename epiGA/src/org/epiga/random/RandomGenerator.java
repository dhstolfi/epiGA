/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.random;

import java.util.Random;

/**
 * Singleton for the random number generator. It is highly recommended to use a
 * better RNG like Mersenne Twister.
 * 
 * @author Daniel H. Stolfi
 * 
 */
public class RandomGenerator {

	private static Random rand = null;

	private RandomGenerator() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Initializes the RNG.
	 * 
	 * @param seed the seed for the RNG
	 */
	public static void init(long seed) {
		rand = new Random(seed);
	}

	/**
	 * Return the next pseudorandom float value [0.0, 1.0) in the sequence.
	 * 
	 * @return a pseudorandom float value
	 */
	public static Float nextFloat() {
		return rand.nextFloat();
	}

	/**
	 * Return the next pseudorandom integer value [0, bound) in the sequence.
	 * 
	 * @param bound the upper bound
	 * @return a pseudorandom int value
	 */
	public static int nextInt(int bound) {
		return rand.nextInt(bound);
	}

	/**
	 * Return the next pseudorandom boolean value in the sequence.
	 * 
	 * @return a pseudorandom boolean value
	 */
	public static boolean nextBoolean() {
		return rand.nextBoolean();
	}
}
