/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.representation;

import java.util.BitSet;

/**
 * Binary representation including father and mother vectors.
 * 
 * @author Daniel H. Stolfi
 * 
 */
public class BinaryFamilyRepresentation extends BinaryRepresentation {

	protected BitSet f;
	protected BitSet m;

	/**
	 * Constructor. Creates a new empty Binary representation including a solution
	 * vector, mother and father.
	 * 
	 * @param size the size of the Binary representation
	 */
	public BinaryFamilyRepresentation(int size) {
		super(size);
		f = (BitSet) x.clone();
		m = (BitSet) x.clone();
	}

	/**
	 * Constructor. Creates a new Binary representation and sets the solution
	 * vector, mother and father.
	 * 
	 * @param x    the solution vector
	 * @param size the size of the Binary representation
	 */
	public BinaryFamilyRepresentation(BitSet x, int size) {
		super(x, size);
		f = (BitSet) this.x.clone();
		m = (BitSet) this.x.clone();
	}

	/**
	 * Constructor. Creates a new Binary representation containing father and mother
	 * which are equals to the solution vector.
	 * 
	 * @param x the solution vector
	 */
	public BinaryFamilyRepresentation(boolean[] x) {
		super(x);
		f = (BitSet) this.x.clone();
		m = (BitSet) this.x.clone();
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(super.toString());
		str.append("f: ");
		for (int i = 0; i < size; i++) {
			str.append(f.get(i) ? "1" : "0");
		}
		str.append("m: ");
		for (int i = 0; i < size; i++) {
			str.append(m.get(i) ? "1" : "0");
		}
		return str.toString();
	}

	/**
	 * Generates a random new instance of the problem.
	 */
	@Override
	public void generate() {
		super.generate();
		f = (BitSet) x.clone();
		m = (BitSet) x.clone();
	}

	/**
	 * Returns the father vector.
	 * 
	 * @return the father vector.
	 */
	public BitSet getFather() {
		return f;
	}

	/**
	 * Sets the father vector.
	 * 
	 * @param father the father vector to set.
	 */
	public void setFather(BitSet father) {
		f = (BitSet) father.clone();
	}

	/**
	 * Returns the mother vector.
	 * 
	 * @return the mother vector.
	 */
	public BitSet getMother() {
		return m;
	}

	/**
	 * Sets the mother vector.
	 * 
	 * @param mother the mother vector to set.
	 */
	public void setMother(BitSet mother) {
		m = (BitSet) mother.clone();
	}

	@Override
	public Representation<BitSet> duplicate() {
		BinaryFamilyRepresentation br = new BinaryFamilyRepresentation(size);
		br.x = (BitSet) x.clone();
		br.f = (BitSet) f.clone();
		br.m = (BitSet) m.clone();
		return br;
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return x.hashCode();
	}

}
