/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.representation.operators;

import java.util.BitSet;

/**
 * Epigenetic operator which implements the nucleosome based reproduction of two
 * Integer vectors.
 * 
 * @author Daniel H. Stolfi
 * 
 */
public class IntegerNucleosomeBasedReproduction {

	/**
	 * Nucleosome based reproduction of two Integer vectors.
	 * 
	 * @param x1 the first Integer vector.
	 * @param x2 the second Integer vector.
	 * @param n  the nucleosome mask.
	 */
	public void reproduction(int[] x1, int[] x2, BitSet n) {
		for (int i = 0; i < x1.length; ++i) {
			if (!n.get(i)) {
				int temp = x1[i];
				x1[i] = x2[i];
				x2[i] = temp;
			}
		}
	}
}
