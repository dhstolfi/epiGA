/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.representation.operators;

import java.util.BitSet;

import org.epiga.random.RandomGenerator;

/**
 * Epigenetic operator which implements the epigenetic mechanism Gene Silencing
 * on Integer vectors.
 * 
 * @author Daniel H. Stolfi
 * 
 */
public class IntegerGeneSilencing {

	/**
	 * Gene Silencing on Integer vectors.
	 * 
	 * @param x            the solution vector.
	 * @param min          the minimum integer value.
	 * @param max          the maximum integer value.
	 * @param delta        the delta integer value.
	 * @param n            the nucleosome vector.
	 * @param environment  the environment.
	 * @param eprobability the epigenetic probability.
	 */
	public void methilation(int[] x, int min, int max, int delta, BitSet n, float[] environment, float eprobability) {
		for (int i = 0; i < x.length; i++) {
			if (n.get(i) && RandomGenerator.nextFloat() < eprobability) {
				if (RandomGenerator.nextFloat() < environment[i]) {
					delta = -delta;
				}
				x[i] = Math.max(min, Math.min(max, x[i] + delta));
			}
		}

	}

}
