/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.representation.operators;

import java.util.BitSet;

/**
 * Epigenetic operator which implements the nucleosome based reproduction of two
 * Binary vectors.
 * 
 * @author Daniel H. Stolfi
 * 
 */
public class BinaryNucleosomeBasedReproduction {

	/**
	 * Nucleosome based reproduction of two Binary vectors.
	 * 
	 * @param x1   the first binary vector.
	 * @param x2   the second binary vector.
	 * @param n    the nuclesome mask.
	 * @param size the size of the vectors.
	 */
	public void reproduction(BitSet x1, BitSet x2, BitSet n, int size) {
		for (int i = 0; i < size; ++i) {
			if (!n.get(i)) {
				boolean temp = x1.get(i);
				x1.set(i, x2.get(i));
				x2.set(i, temp);
			}
		}
	}
}
