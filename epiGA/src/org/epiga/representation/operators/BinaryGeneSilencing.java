/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.representation.operators;

import java.util.BitSet;

import org.epiga.random.RandomGenerator;

/**
 * Epigenetic operator which implements the epigenetic mechanism Gene Silencing
 * on Binary vectors.
 * 
 * @author Daniel H. Stolfi
 * 
 */
public class BinaryGeneSilencing {

	/**
	 * Gene Silencing on Binary vectors.
	 * 
	 * @param x            the solution vector
	 * @param n            the nucleosome mask
	 * @param environment  the environment
	 * @param eprobability the epigenetic probability
	 */
	public void methilation(BitSet x, BitSet n, float[] environment, float eprobability) {
		int size = environment.length;
		for (int i = 0; i < size; i++) {
			if (n.get(i) && RandomGenerator.nextFloat() < eprobability) {
				x.set(i, (RandomGenerator.nextFloat() < environment[i]));
			}
		}
	}

}
