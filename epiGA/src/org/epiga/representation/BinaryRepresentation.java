/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.representation;

import java.util.BitSet;

import org.epiga.random.RandomGenerator;

/**
 * Binary representation.
 * 
 * @author Daniel H. Stolfi
 * 
 */
public class BinaryRepresentation extends Representation<BitSet> {

	protected BitSet x;
	protected int size;

	/**
	 * Constructor. Creates a new empty Binary representation
	 * 
	 * @param size the size of the Binary representation
	 */
	public BinaryRepresentation(int size) {
		x = new BitSet(size);
		this.size = size;
	}

	/**
	 * Constructor. Creates a new Binary representation and sets the solution vector
	 * 
	 * @param x    the solution vector
	 * @param size the size of the Binary representation
	 */
	public BinaryRepresentation(BitSet x, int size) {
		this(size);
		this.x = (BitSet) x.clone();
	}

	/**
	 * Constructor. Creates a new Binary representation.
	 * 
	 * @param x the solution vector
	 */
	public BinaryRepresentation(boolean[] x) {
		this(x.length);
		for (int i = 0; i < x.length; i++) {
			this.x.set(i, x[i]);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BinaryRepresentation) {
			BinaryRepresentation br = (BinaryRepresentation) obj;
			return x.equals(br.x);
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < size; i++) {
			str.append(x.get(i) ? "1" : "0");
		}
		return str.toString();
	}

	/**
	 * Generates a random new instance of the problem.
	 */
	public void generate() {
		for (int i = 0; i < size; ++i) {
			x.set(i, RandomGenerator.nextBoolean());
		}
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public BitSet getSolution() {
		return x;
	}

	@Override
	public void setSolution(BitSet x) {
		this.x = (BitSet) x.clone();
	}

	@Override
	public Representation<BitSet> duplicate() {
		BinaryRepresentation br = new BinaryRepresentation(size);
		br.x = (BitSet) x.clone();
		return br;
	}

	@Override
	public int hashCode() {
		return x.hashCode();
	}

}
