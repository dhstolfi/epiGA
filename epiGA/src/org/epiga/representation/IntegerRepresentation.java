/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.representation;

import java.util.Arrays;

import org.epiga.random.RandomGenerator;

/**
 * Integer representation.
 * 
 * @author Daniel H. Stolfi
 * 
 */
public class IntegerRepresentation extends Representation<int[]> {

	protected int[] x;

	/**
	 * Constructor. Creates a new empty Integer representation
	 * 
	 * @param size the size of the Integer representation
	 */
	public IntegerRepresentation(int size) {
		x = new int[size];
	}

	/**
	 * Constructor. Creates a new Integer representation and sets the solution
	 * vector
	 * 
	 * @param x the solution vector
	 */
	public IntegerRepresentation(int[] x) {
		this.x = Arrays.copyOf(x, x.length);
	}

	/**
	 * Generates a random instance of the problem
	 * 
	 * @param min the minimum int
	 * @param max the maximum int
	 */
	public void generate(int min, int max) {
		for (int i = 0; i < x.length; i++) {
			x[i] = min + RandomGenerator.nextInt(max - min + 1);
		}

	}

	@Override
	public int getSize() {
		return x.length;
	}

	@Override
	public int[] getSolution() {
		return x;
	}

	@Override
	public void setSolution(int[] x) {
		this.x = x;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof IntegerRepresentation) {
			IntegerRepresentation ir = (IntegerRepresentation) obj;
			return Arrays.equals(x, ir.x);
		}
		return false;
	}

	@Override
	public String toString() {
		return Arrays.toString(x);
	}

	@Override
	public Representation<int[]> duplicate() {
		return new IntegerRepresentation(x);
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(x);
	}

}
