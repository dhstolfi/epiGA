/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.representation;

/**
 * Base class for defining representations.
 * 
 * @author Daniel H. Stolfi
 * @param <T> the base type for the representation.
 * 
 */
public abstract class Representation<T> {

	/**
	 * Returns the size of the solution vector.
	 * 
	 * @return the size of the solution vector.
	 */
	public abstract int getSize();

	/**
	 * Returns the solution vector.
	 * 
	 * @return the solution vector.
	 */
	public abstract T getSolution();

	/**
	 * Replaces the solution vector.
	 * 
	 * @param x the solution vector.
	 */
	public abstract void setSolution(T x);

	/**
	 * Duplicates the representation.
	 * 
	 * @return the duplicated representation.
	 */
	public abstract Representation<T> duplicate();

}
