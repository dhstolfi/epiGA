/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.problem;

import java.io.FileNotFoundException;
import java.util.BitSet;
import java.util.Hashtable;

import org.epiga.representation.Representation;

/**
 * The base class for defining problems.
 * 
 * @author Daniel H. Stolfi
 * @param <R> the representation type
 */
public abstract class Problem<R extends Representation<?>> {

	protected int evaluations = 0;
	protected float[] environment;
	protected int size;
	private Hashtable<R, Double> table;
	private boolean cache;

	private static final String ENVIRONMENTFORMAT = "%4.3f";
	private static final int TABLECAPACITY = 1000;

	/**
	 * Constructor.
	 * 
	 * @param size  the problem size.
	 * @param cache <b>true</b> if a hash table will be used as a evaluation cache.
	 */
	public Problem(int size, boolean cache) {
		this.size = size;
		this.cache = cache;
		environment = new float[size];
		for (int i = 0; i < environment.length; i++) {
			environment[i] = 0.5f;
		}
		if (cache) {
			table = new Hashtable<>(TABLECAPACITY);
		}
	}

	/**
	 * Generate a new solution of the Problem.
	 * 
	 * @return the new solution generated.
	 */
	public abstract R generateSolution();

	/**
	 * Returns the String representation of the Problem
	 */
	public String toString() {
		StringBuilder str = new StringBuilder("Size: " + size);
		str.append("\nCache: " + cache);
		str.append("\nEvaluations: " + evaluations);
		str.append("\nEnvironment: ");
		for (int i = 0; i < environment.length; i++) {
			str.append(String.format(ENVIRONMENTFORMAT, environment[i]) + " ");
		}
		return str.toString();
	}

	/**
	 * Compute and return the fitness value.
	 * 
	 * @param representation the representation to be evaluated.
	 * @return the fitness value
	 */
	public double getFitness(R representation) {
		double fitness;
		if (cache) {
			Double value = null;
			synchronized (this) {
				value = table.get(representation);
			}
			if (value != null) {
				fitness = value.doubleValue();
			} else {
				fitness = computeFitness(representation);
				synchronized (this) {
					evaluations++;
					table.put(representation, Double.valueOf(fitness));
				}
			}
		} else {
			synchronized (this) {
				evaluations++;
			}
			fitness = computeFitness(representation);
		}
		return fitness;
	}

	/**
	 * Compute the fitness value fixing the representation if needed.
	 *
	 * @param representation the representation to be used to fitness calculation.
	 * @return the fitness value
	 */
	protected abstract double computeFitness(R representation);

	/**
	 * Returns the number of evaluations done.
	 * 
	 * @return the number of evaluations done.
	 */
	public synchronized int getEvaluations() {
		return evaluations;
	}

	/**
	 * Returns the size of the solution vector of the problem.
	 * 
	 * @return the size of the solution vector of the problem.
	 */
	public int getSize() {
		return size;
	}

	/**
	 * Epigenetic crossover of two solutions.
	 * 
	 * @param r1 the representation of the first solution
	 * @param r2 the representation of the second solution
	 * @param n  the nucleosome vector
	 */
	public abstract void crossover(R r1, R r2, BitSet n);

	/**
	 * Epigenetic methylation of a solution.
	 * 
	 * @param r            the representation of the solution
	 * @param n            the nucleosome vector
	 * @param eprobability the epigenetic probability
	 */
	public abstract void methylate(R r, BitSet n, float eprobability);

	/**
	 * Returns <b>true</b> if the first fitness value is better than the second one,
	 * according to the comparison function.
	 * 
	 * @param d1 the first fitness value
	 * @param d2 the second fitness value
	 * @return <b>true</b> if the first fitness value is better than the second one.
	 */
	public boolean isBetter(double d1, double d2) {
		return compare(d1, d2) <= 0;
	}

	/**
	 * Comparator which defines if it's a minimization or maximization problem.
	 * 
	 * @param d1 the first fitness value
	 * @param d2 the second fitness value
	 * @return <b>true</b> or <b>false</b> depending on the use of
	 *         minimizingComparator of maximizingComparator.
	 */
	public abstract int compare(double d1, double d2);

	/**
	 * Minimization comparator.
	 * 
	 * @param d1 the first fitness value
	 * @param d2 the second fitness value
	 * @return -1 if d1 &lt; d2, 0 if d1 == d2, and +1 if d1 &gt; d2
	 */
	protected int minimizingComparator(double d1, double d2) {
		return Double.compare(d1, d2);
	}

	/**
	 * Maximization comparator.
	 * 
	 * @param d1 the first fitness value
	 * @param d2 the second fitness value
	 * @return +1 if d1 &lt; d2, 0 if d1 == d2, and -1 if d1 &gt; d2
	 */
	protected int maximizingComparator(double d1, double d2) {
		return -1 * Double.compare(d1, d2);
	}

	/**
	 * Saves the problem representation to a file.
	 * 
	 * @param representation the representation
	 * @param file           the filename
	 * @throws FileNotFoundException If the file cannot be saved
	 */
	public abstract void save(R representation, String file) throws FileNotFoundException;

}
