/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.problem.onemax;

import org.epiga.problem.Problem;
import org.epiga.representation.Representation;
import org.epiga.representation.operators.BinaryGeneSilencing;
import org.epiga.representation.operators.BinaryNucleosomeBasedReproduction;

/**
 * Base class for the Onemax Problem
 * 
 * @author Daniel H. Stolfi
 * @param <R> the representation type
 * 
 */
public abstract class AbstractOnemaxProblem<R extends Representation<?>> extends Problem<R> {

	protected BinaryNucleosomeBasedReproduction crossover = new BinaryNucleosomeBasedReproduction();
	protected BinaryGeneSilencing methilation = new BinaryGeneSilencing();

	/**
	 * Constructor
	 * 
	 * @param size the problem size
	 */
	public AbstractOnemaxProblem(int size) {
		super(size, false);
		for (int i = 0; i < environment.length; i++) {
			environment[i] = 1.0f;
		}
	}

	@Override
	public String toString() {
		return "Size: " + Integer.toString(size);
	}

	@Override
	public int compare(double d1, double d2) {
		return minimizingComparator(d1, d2);
	}

}
