/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.problem.onemax;

import java.io.FileNotFoundException;
import java.util.BitSet;

import org.epiga.representation.BinaryFamilyRepresentation;

/**
 * Implementation of Onemax Problem using Family representation
 * 
 * @author Daniel H. Stolfi
 * 
 */
public class FamilyOnemaxProblem extends AbstractOnemaxProblem<BinaryFamilyRepresentation> {

	/**
	 * Constructor
	 * 
	 * @param size the problem size
	 */
	public FamilyOnemaxProblem(int size) {
		super(size);
	}

	@Override
	public void crossover(BinaryFamilyRepresentation r1, BinaryFamilyRepresentation r2, BitSet n) {
		BitSet x1 = r1.getSolution();
		BitSet x2 = r2.getSolution();
		r1.setFather(x1);
		r2.setFather(x1);
		r1.setMother(x2);
		r2.setMother(x2);
		crossover.reproduction(x1, x2, n, size);
	}

	@Override
	public void methylate(BinaryFamilyRepresentation r, BitSet n, float eprobability) {
		methilation.methilation(r.getSolution(), n, environment, eprobability);
	}

	@Override
	public BinaryFamilyRepresentation generateSolution() {
		return new BinaryFamilyRepresentation(size);
	}

	@Override
	protected double computeFitness(BinaryFamilyRepresentation br) {
		return size - (double) br.getSolution().cardinality();
	}

	@Override
	public void save(BinaryFamilyRepresentation representation, String file) throws FileNotFoundException {
		// Nothing here
	}

}
