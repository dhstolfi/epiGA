/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.problem.onemax;

import java.io.FileNotFoundException;
import java.util.BitSet;

import org.epiga.random.RandomGenerator;
import org.epiga.representation.BinaryRepresentation;

/**
 * Implementation of Onemax Problem using binary representation
 * 
 * @author Daniel H. Stolfi
 * 
 */
public class OnemaxProblem extends AbstractOnemaxProblem<BinaryRepresentation> {

	/**
	 * Constructor
	 * 
	 * @param size the problem size
	 */
	public OnemaxProblem(int size) {
		super(size);
	}

	@Override
	protected double computeFitness(BinaryRepresentation br) {
		return size - (double) br.getSolution().cardinality();
	}

	@Override
	public void crossover(BinaryRepresentation r1, BinaryRepresentation r2, BitSet n) {
		crossover.reproduction(r1.getSolution(), r2.getSolution(), n, size);
	}

	@Override
	public void methylate(BinaryRepresentation r, BitSet n, float eprobability) {
		methilation.methilation(r.getSolution(), n, environment, eprobability);
	}

	@Override
	public BinaryRepresentation generateSolution() {
		boolean[] x = new boolean[size];
		for (int i = 0; i < x.length; i++) {
			x[i] = RandomGenerator.nextBoolean();
		}
		return new BinaryRepresentation(x);
	}

	@Override
	public void save(BinaryRepresentation representation, String file) throws FileNotFoundException {
		// Nothing here
	}

}
