/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.problem.onemax;

import java.io.IOException;

import org.epiga.algorithm.EpigeneticAlgorithm;
import org.epiga.algorithm.EpigeneticException;

/**
 * Example of optimization: executes the epigenetic optimization which maximizes
 * the number of ones in a vector.
 * 
 * @author Daniel H. Stolfi
 * 
 */
public class OnemaxEpiGA {

	/**
	 * Entry point.<br>
	 * Example: OnemaxEpiGA 20000 500 1 0.08 0.08 3 100
	 * 
	 * @param args Evaluations Individuals Cells Pe Pn R SIZE Vb OUT
	 * @throws IOException          if any file cannot be read or written.
	 * @throws InterruptedException if it's interrupted.
	 * @throws EpigeneticException  if something goes wrong.
	 */
	public static void main(String[] args) throws IOException, InterruptedException, EpigeneticException {
		if (args.length < 7) {
			System.err.println("Onemax epiGA");
			System.err.println("Daniel H. Stolfi and Enrique Alba\n");
			System.err.println("Params: Evaluations Individuals Cells Pe Pn R SIZE Vb OUT");
			System.exit(1);
		} else {
			int nEvaluations = Integer.parseInt(args[0]);
			int nIndividuals = Integer.parseInt(args[1]);
			int nCells = Integer.parseInt(args[2]);
			float pe = Float.parseFloat(args[3]);
			float pn = Float.parseFloat(args[4]);
			int radius = Integer.parseInt(args[5]);
			int size = Integer.parseInt(args[6]);

			int vb = 0;
			if (args.length > 7) {
				vb = Integer.parseInt(args[7]);
			}

			String out = null;
			if (args.length > 8) {
				out = args[8];
			}

			OnemaxProblem problem = new OnemaxProblem(size);

			EpigeneticAlgorithm<?, ?> ea = new EpigeneticAlgorithm<>(problem, nIndividuals, nCells, pe, pn, radius);

			ea.run(nEvaluations, 0, 1, null, null, out, vb);
		}
	}
}
