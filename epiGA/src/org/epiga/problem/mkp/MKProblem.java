/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.problem.mkp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.BitSet;
import java.util.StringTokenizer;

import org.epiga.problem.Problem;
import org.epiga.random.RandomGenerator;
import org.epiga.representation.BinaryRepresentation;
import org.epiga.representation.operators.BinaryGeneSilencing;
import org.epiga.representation.operators.BinaryNucleosomeBasedReproduction;

/**
 * Multidemensional Knapsack Problem (OR-Library or SAC-94).
 * 
 * @author Daniel H. Stolfi
 * 
 */
public class MKProblem extends Problem<BinaryRepresentation> {

	private double[] p; // profits
	private int[][] r; // restrictions
	private int[] b; // capacity
	private double maxp; // maximum profit (penalizing)
	private Double optimum;

	private static final int COLS = 10;
	private static final String FORMAT = "%8d";
	private static final String FORMAT2 = "%8.1f";

	protected BinaryNucleosomeBasedReproduction crossover = new BinaryNucleosomeBasedReproduction();
	protected BinaryGeneSilencing methilation = new BinaryGeneSilencing();

	/**
	 * Constructor. Parses SAC-94
	 * 
	 * @param filename the name of the file containing the instance
	 * 
	 * @throws IOException if the file cannot be read.
	 */
	public MKProblem(String filename) throws IOException {
		super(0, false);
		try (FileReader fr = new FileReader(filename); BufferedReader br = new BufferedReader(fr)) {
			StringTokenizer st;
			String buf = br.readLine();
			st = new StringTokenizer(buf);
			int m = Integer.parseInt(st.nextToken());
			int n = Integer.parseInt(st.nextToken());
			size = n;

			p = new double[n];
			r = new int[m][n];
			b = new int[m];

			// p
			int i = 0;
			maxp = 0.0d;
			for (int ii = 0; ii < Math.ceil(n / 10.0); ii++) {
				buf = br.readLine();
				st = new StringTokenizer(buf);
				while (st.hasMoreTokens()) {
					p[i] = Double.parseDouble(st.nextToken());
					maxp += p[i];
					i++;
				}
			}

			// b
			int j = 0;
			for (int jj = 0; jj < Math.ceil(m / 10.0); jj++) {
				buf = br.readLine();
				st = new StringTokenizer(buf);
				while (st.hasMoreTokens()) {
					b[j] = Integer.parseInt(st.nextToken());
					j++;
				}
			}

			// r
			for (j = 0; j < m; j++) {
				i = 0;
				for (int ii = 0; ii < Math.ceil(n / 10.0); ii++) {
					buf = br.readLine();
					st = new StringTokenizer(buf);
					while (st.hasMoreTokens()) {
						r[j][i] = Integer.parseInt(st.nextToken());
						i++;
					}
				}
			}

			buf = br.readLine();
			optimum = -1.0d * Double.parseDouble(br.readLine());

		} catch (IOException ex) {
			System.err.println(ex.toString());
		}

		calculateEnvironment();

	}

	private void calculateEnvironment() {
		int n = p.length;
		int m = b.length;
		environment = new float[n];
		float max1 = Float.MIN_VALUE;
		float min1 = Float.MAX_VALUE;
		for (int i = 0; i < n; i++) {
			float ac2 = 0.0f;
			for (int j = 0; j < m; j++) {
				ac2 += r[j][i];
			}
			if (ac2 == 0) {
				environment[i] = 0;
			} else {
				environment[i] = (float) (p[i] / ac2);
			}
			max1 = Math.max(environment[i], max1);
			min1 = Math.min(environment[i], min1);
		}
		max1 -= min1;
		max1 *= 3.0f;
		for (int i = 0; i < n; i++) {
			environment[i] -= min1;
			environment[i] /= max1;
			environment[i] += 0.3333333f;
		}
	}

	/**
	 * Constructor. Parses OR-Library
	 * 
	 * @param filename the name of the file containing the instances
	 * @param which    the instance to load
	 * @throws IOException if the file cannot be read.
	 */
	public MKProblem(String filename, int which) throws IOException {
		super(0, false);
		try (FileReader fr = new FileReader(filename); BufferedReader br = new BufferedReader(fr)) {
			StringTokenizer st;
			String buf = br.readLine();
			int problem = 1;

			while (problem <= which) {
				buf = br.readLine();
				while (buf.trim().equals("")) {
					buf = br.readLine();
				}
				st = new StringTokenizer(buf);
				int n = Integer.parseInt(st.nextToken());
				int m = Integer.parseInt(st.nextToken());
				optimum = 1.0d * Double.parseDouble(st.nextToken());
				if (optimum.doubleValue() == 0.0d) {
					optimum = null;
				}

				p = new double[n];
				r = new int[m][n];
				b = new int[m];
				size = n;

				// p
				int i = 0;
				maxp = 0.0d;
				while (i < n) {
					buf = br.readLine();
					st = new StringTokenizer(buf);
					while (st.hasMoreTokens()) {
						p[i] = Double.parseDouble(st.nextToken());
						maxp += p[i];
						i++;
					}
				}

				// r
				i = 0;
				int j = 0;
				while (j < m) {
					buf = br.readLine();
					st = new StringTokenizer(buf);
					while (st.hasMoreTokens()) {
						r[j][i] = Integer.parseInt(st.nextToken());
						i++;
						if (i == n) {
							i = 0;
							j++;
						}
					}
				}

				// b
				j = 0;
				while (j < m) {
					buf = br.readLine();
					st = new StringTokenizer(buf);
					while (st.hasMoreTokens()) {
						b[j] = Integer.parseInt(st.nextToken());
						j++;
					}
				}
				problem++;
			}
		} catch (IOException ex) {
			System.err.println(ex.toString());
		}

		calculateEnvironment();
	}

	@Override
	public String toString() {

		int m = b.length;
		int n = p.length;

		StringBuilder str = new StringBuilder("Multidimensional Knapsack\n");
		str.append("n: " + n + " m: " + m);

		if (optimum != null) {
			str.append(" o: " + optimum);
		}

		str.append("\np:\n");
		for (int pp = 0; pp < n; pp++) {
			str.append(String.format(FORMAT2, p[pp]));
			if ((pp + 1) % COLS == 0) {
				str.append('\n');
			}
		}
		str.append("\nb:\n");
		for (int bb = 0; bb < m; bb++) {
			str.append(String.format(FORMAT, b[bb]));
			if ((bb + 1) % COLS == 0) {
				str.append('\n');
			}
		}
		str.append("\nr:\n");
		for (int jj = 0; jj < m; jj++) {
			for (int rr = 0; rr < n; rr++) {
				str.append(String.format(FORMAT, r[jj][rr]));
				if ((rr + 1) % COLS == 0) {
					str.append('\n');
				}
			}
			str.append('\n');
		}

		return str.toString();
	}

	@Override
	protected double computeFitness(BinaryRepresentation rep) {
		double fitness = compute(rep);

		while (fitness < 0.0d) { // Invalid
			BitSet x = rep.getSolution();
			int pos = -1;
			while (pos == -1) {
				pos = x.nextSetBit(RandomGenerator.nextInt(size));
			}
			x.clear(pos);
			fitness = compute(rep);
		}
		return fitness;
	}

	private double compute(BinaryRepresentation rep) {
		BitSet x = rep.getSolution();
		double fitness = 0.0d;
		int[] temp = new int[b.length];
		for (int j = 0; j < temp.length; j++) {
			temp[j] = 0;
		}
		for (int i = 0; i < p.length; i++) {
			if (x.get(i)) {
				fitness += p[i];
				for (int j = 0; j < temp.length; j++) {
					temp[j] += r[j][i];
				}
			}
		}

		boolean ok = true;
		int j = 0;
		while (j < b.length && ok) {
			ok = temp[j] <= b[j];
			j++;
		}
		if (!ok) {
			fitness -= maxp;
		}

		return fitness;
	}

	/**
	 * Saves the problem instance to CPLEX format. Useful to compare results.
	 * 
	 * @param filename the file.
	 * @param name     the instance name.
	 * @throws IOException if the file cannot be saved.
	 */
	public void saveCPLEX(String filename, String name) throws IOException {
		try (FileWriter f = new FileWriter(filename)) {
			f.write("SET TIMELIMIT 3600\n\n");
			f.write("ENTER " + name + "\n\n");
			f.write("MAXIMIZE\n  obj: ");
			for (int i = 0; i < p.length; i++) {
				f.write(p[i] + " x" + (i + 1) + " ");
				if (i < p.length - 1) {
					f.write("+ ");
				}
			}
			f.write("\n\nSUBJECT TO");
			for (int j = 0; j < b.length; j++) {
				f.write("\n  c" + (j + 1) + ": ");
				for (int i = 0; i < p.length; i++) {
					f.write(r[j][i] + " x" + (i + 1) + " ");
					if (i < p.length - 1) {
						f.write("+ ");
					}
				}
				f.write("<= " + b[j]);
			}
			f.write("\n\nBOUNDS\n");
			for (int i = 0; i < p.length; i++) {
				f.write("  0 <= x" + (i + 1) + " <= 1\n");
			}
			f.write("\nBINARY\n  ");
			for (int i = 0; i < p.length; i++) {
				f.write("x" + (i + 1) + " ");
			}
			f.write("\nEND\n");
			f.write("\nOPTIMIZE\n");
			f.write("\nDISPLAY SOLUTION VARIABLES * \n\n");
			f.write("\nDISPLAY SOLUTION LIST * \n\n");
		} catch (IOException ex) {
			System.err.println(ex.toString());
		}
	}

	/**
	 * Used to convert to CPLEX format.
	 * 
	 * @param args mkpfile cplexfile name [instance]
	 * @throws NumberFormatException if the instance number is not a valid number
	 * @throws Exception             if something goes wrong
	 */
	public static void main(String[] args) throws Exception {
		if (args.length < 3) {
			System.err.println("Params: mkpfile cplexfile name [instance]");
			System.exit(1);
		} else {
			MKProblem p;
			if (args.length > 3) {
				p = new MKProblem(args[0], Integer.parseInt(args[3]));
			} else {
				p = new MKProblem(args[0]);
			}
			p.saveCPLEX(args[1], args[2]);
			System.out.println(p);
		}
	}

	@Override
	public BinaryRepresentation generateSolution() {
		BinaryRepresentation br = new BinaryRepresentation(getSize());
		br.generate();
		return br;
	}

	@Override
	public void crossover(BinaryRepresentation r1, BinaryRepresentation r2, BitSet n) {
		crossover.reproduction(r1.getSolution(), r2.getSolution(), n, size);
	}

	@Override
	public void methylate(BinaryRepresentation r, BitSet n, float eprobability) {
		methilation.methilation(r.getSolution(), n, environment, eprobability);
	}

	@Override
	public int compare(double d1, double d2) {
		return maximizingComparator(d1, d2);
	}

	@Override
	public void save(BinaryRepresentation representation, String file) throws FileNotFoundException {
		// Nothing here
	}

}
