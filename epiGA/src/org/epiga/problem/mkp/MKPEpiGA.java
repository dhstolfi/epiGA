/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.problem.mkp;

import java.io.IOException;

import org.epiga.algorithm.EpigeneticAlgorithm;
import org.epiga.algorithm.EpigeneticException;

/**
 * Executes the epigenetic optimization of the MKP (OR-Library).
 * 
 * @author Daniel H. Stolfi
 * 
 */
public class MKPEpiGA {

	/**
	 * Entry point.<br>
	 * Example: MKPEpiGA 1000000 400 1 0.01 0.10 2 ./bin/mknapcb1.txt 1
	 * 
	 * @param args Evaluations Individuals Cells Pe Pn R FILE Which Seconds Vb OUT
	 * @throws IOException          if something goes wrong
	 * @throws InterruptedException if something goes wrong
	 * @throws EpigeneticException  if something goes wrong
	 */
	public static void main(String[] args) throws IOException, InterruptedException, EpigeneticException {
		if (args.length < 8) {
			System.err.println("MKP epiGA");
			System.err.println("Daniel H. Stolfi and Enrique Alba\n");
			System.err.println("Params: Evaluations Individuals Cells Pe Pn R FILE Which Seconds Vb OUT");
			System.exit(1);
		} else {
			int nEvaluations = Integer.parseInt(args[0]);
			int nIndividuals = Integer.parseInt(args[1]);
			int nCells = Integer.parseInt(args[2]);
			float pe = Float.parseFloat(args[3]);
			float pn = Float.parseFloat(args[4]);
			int radius = Integer.parseInt(args[5]);
			String file = args[6];
			int which = Integer.parseInt(args[7]);

			int seconds = 0;
			if (args.length > 8) {
				seconds = Integer.parseInt(args[8]);
			}

			int vb = 0;
			if (args.length > 9) {
				vb = Integer.parseInt(args[9]);
			}

			String out = null;
			if (args.length > 10) {
				out = args[10];
			}

			MKProblem problem;
			if (which == 0) {
				problem = new MKProblem(file);
			} else {
				problem = new MKProblem(file, which);
			}

			EpigeneticAlgorithm<?, ?> ea = new EpigeneticAlgorithm<>(problem, nIndividuals, nCells, pe, pn, radius);

			ea.run(nEvaluations, seconds, 1, null, null, out, vb);

		}
	}
}