/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.problem.intmax;

import org.epiga.algorithm.EpigeneticAlgorithm;
import org.epiga.representation.IntegerRepresentation;

/**
 * Example of continuous optimization: executes the epigenetic optimization
 * which maximizes the summation of the numbers in a vector.
 * 
 * @author Daniel H. Stolfi
 * 
 */
public class IntmaxEpiGA {

	/**
	 * Entry point.<br>
	 * Example: IntmaxEpiGA 100000 500 1 0.05 0.08 3 100
	 * 
	 * @param args Evaluations Individuals Cells Pe Pn R SIZE Seconds Vb OUT
	 * @throws Exception if something goes wrong.
	 */
	public static void main(String[] args) throws Exception {
		if (args.length < 9) {
			System.err.println("Intmax epiGA");
			System.err.println("Daniel H. Stolfi and Enrique Alba\n");
			System.err.println("Params: Evaluations Individuals Cells Pe Pn R SIZE MIN MAX Seconds Vb OUT");
			System.exit(1);
		} else {
			int nEvaluations = Integer.parseInt(args[0]);
			int nIndividuals = Integer.parseInt(args[1]);
			int nCells = Integer.parseInt(args[2]);
			float pe = Float.parseFloat(args[3]);
			float pn = Float.parseFloat(args[4]);
			int radius = Integer.parseInt(args[5]);
			int size = Integer.parseInt(args[6]);
			int min = Integer.parseInt(args[7]);
			int max = Integer.parseInt(args[8]);

			int seconds = 0;
			if (args.length > 9) {
				seconds = Integer.parseInt(args[9]);
			}

			int vb = 0;
			if (args.length > 10) {
				vb = Integer.parseInt(args[10]);
			}

			String out = null;
			if (args.length > 11) {
				out = args[11];
			}

			IntmaxProblem problem = new IntmaxProblem(size, min, max);

			EpigeneticAlgorithm<IntmaxProblem, IntegerRepresentation> ea = new EpigeneticAlgorithm<>(problem,
					nIndividuals, nCells, pe, pn, radius);

			ea.run(nEvaluations, seconds, 1, null, null, out, vb);
		}
	}
}
