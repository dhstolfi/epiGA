/**
 * Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics. 
 * In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
 * doi> 10.1016/j.ins.2017.10.005
 */
package org.epiga.problem.intmax;

import java.io.FileNotFoundException;
import java.util.BitSet;

import org.epiga.algorithm.EpigeneticException;
import org.epiga.problem.Problem;
import org.epiga.random.RandomGenerator;
import org.epiga.representation.IntegerRepresentation;
import org.epiga.representation.operators.IntegerGeneSilencing;
import org.epiga.representation.operators.IntegerNucleosomeBasedReproduction;

/**
 * Example of continuous optimization.
 * 
 * @author Daniel H. Stolfi
 * 
 */
public class IntmaxProblem extends Problem<IntegerRepresentation> {

	private int max;
	private int min;
	private IntegerNucleosomeBasedReproduction reproduction = new IntegerNucleosomeBasedReproduction();
	private IntegerGeneSilencing methilation = new IntegerGeneSilencing();

	private static final int DELTA = 5;

	/**
	 * Constructor.
	 * 
	 * @param size the problem size
	 * @param min  the minimum allowed integer
	 * @param max  the maximum allowed integer
	 * @throws EpigeneticException if something goes wrong
	 */
	public IntmaxProblem(int size, int min, int max) throws EpigeneticException {
		super(size, false);
		if (min > max)
			throw new EpigeneticException("min < max");
		this.max = max;
		this.min = min;
		for (int i = 0; i < environment.length; i++) {
			environment[i] = 0.5f; // Increment/Decrement probability
		}
	}

	@Override
	public String toString() {
		return "Size: " + Integer.toString(size) + " Min: " + Integer.toString(min) + " Max: " + Integer.toString(max);
	}

	@Override
	protected double computeFitness(IntegerRepresentation rep) {
		double fitness = 0;
		int[] x = rep.getSolution();
		for (int i = 0; i < x.length; i++) {
			fitness += x[i];
		}
		return max * size - fitness;
	}

	@Override
	public void crossover(IntegerRepresentation r1, IntegerRepresentation r2, BitSet n) {
		reproduction.reproduction(r1.getSolution(), r2.getSolution(), n);
	}

	@Override
	public void methylate(IntegerRepresentation r, BitSet n, float eprobability) {
		methilation.methilation(r.getSolution(), min, max, DELTA, n, environment, eprobability);
	}

	@Override
	public IntegerRepresentation generateSolution() {
		IntegerRepresentation ir = new IntegerRepresentation(size);
		int[] x = ir.getSolution();
		for (int i = 0; i < size; ++i) {
			x[i] = min + RandomGenerator.nextInt(max + 1 - min);
		}
		return ir;
	}

	@Override
	public int compare(double d1, double d2) {
		return minimizingComparator(d1, d2);
	}

	@Override
	public void save(IntegerRepresentation representation, String file) throws FileNotFoundException {
		// Nothing here
	}

}
