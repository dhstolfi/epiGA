Epigenetic Algorithm (epiGA)
============================

Evolutionary algorithm for combinatorial optimization.

JAVA implementation. **Python** version available [here](https://gitlab.com/dhstolfi/epiGA.py/).

Daniel H. Stolfi and Enrique Alba.
**Epigenetic algorithms: A New way of building GAs based on epigenetics.**
*In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.*
doi> 10.1016/j.ins.2017.10.005

Contents:
---------

- /bin: Binaries.
  - epiGA.jar: The epiGenetic Algorithm, including three test problems.
  - om.sh: Optimization of the Onemax Problem.
  - im.sh: Optimization of the Intmax Problem.
  - mkp.sh: Optimizes one instance of the Multidimesional Knapsack Problem.
  - mknapcb1.txt: MKP instances from OR-Library (J.E. Beasley, 1990).
- /doc: Java docs.
- /src: Source code.

Examples:
---------

```
cd bin
./mkp.sh   # Multidimensional Knapsack Problem (Maximization, Binary Representation).
./om.sh    # OneMax problem (Minimization, Binary Representation).
./im.sh    # IntMax problem (Minimization, Integer Representation).
```

Acknowledgments:
----------------

This research is partially funded by the Spanish MINECO project TIN2014-57341-R (http://moveon.lcc.uma.es). Daniel H. Stolfi is supported by a FPU grant (FPU13/00954) from the Spanish Ministry of Education, Culture and Sports.


